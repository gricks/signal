module gitee.com/gricks/signal

go 1.13

require (
	gitee.com/gricks/logrus v0.0.0-20191126040458-e7b82ef1ff34
	gitee.com/gricks/utils v0.0.0-20191126044549-bf4a152e8850
)
