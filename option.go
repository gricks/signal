package signal

import "gitee.com/gricks/logrus"

type Option func(*Signal)

func WithDup(dup bool) Option {
	return func(sig *Signal) {
		sig.dup = dup
	}
}

func WithLogger(logger *logrus.Entry) Option {
	return func(sig *Signal) {
		sig.logger = logger
	}
}

func WithSignalTerm(f func()) Option {
	return func(sig *Signal) {
		sig.signalTerm = f
	}
}

func WithSignalUsr1(f func()) Option {
	return func(sig *Signal) {
		sig.signalUsr1 = f
	}
}

func WithSignalUsr2(f func()) Option {
	return func(sig *Signal) {
		sig.signalUsr2 = f
	}
}
