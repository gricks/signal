package signal

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"os/signal"
	"path"
	"path/filepath"
	"runtime"
	"strings"
	"syscall"

	"gitee.com/gricks/logrus"
)

type Signal struct {
	signalTerm func()
	signalUsr1 func()
	signalUsr2 func()

	dup      bool
	execDir  string
	execFile string
	logger   *logrus.Entry
}

func NewSignal(opts ...Option) *Signal {
	arg0, err := exec.LookPath(os.Args[0])
	if err != nil {
		panic(err)
	}
	absExecFile, err := filepath.Abs(arg0)
	if err != nil {
		panic(err)
	}
	execDir, execFile := filepath.Split(absExecFile)
	sig := &Signal{
		execDir:  execDir,
		execFile: execFile,
	}
	for _, opt := range opts {
		opt(sig)
	}
	return sig
}

func (this *Signal) Handle() {
	fpid := this.pidfile()
	err := ioutil.WriteFile(fpid, []byte(fmt.Sprintf("%d", os.Getpid())), 0600)
	if err == nil {
		defer os.Remove(fpid)
	}

	// dup stdout & stderr
	// if unexpected panic log file will not to be removed
	if this.dup {
		fout := fmt.Sprintf("%s/%s.%d", this.execDir, this.execFile, os.Getpid())
		logFile, err := os.OpenFile(fout,
			os.O_WRONLY|os.O_CREATE|os.O_SYNC, 0644)
		if err == nil {
			defer os.Remove(fout)
			syscall.Dup2(int(logFile.Fd()), 1)
			syscall.Dup2(int(logFile.Fd()), 2)
			defer logFile.Close()
		}
	}

	c := make(chan os.Signal)
	signal.Notify(c, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGUSR1, syscall.SIGUSR2)
	for {
		s, ok := <-c
		if !ok {
			return
		}
		switch s {
		case syscall.SIGINT, syscall.SIGTERM:
			this.safetyCall(this.signalTerm)
			return
		case syscall.SIGUSR1:
			this.safetyCall(this.signalUsr1)
		case syscall.SIGUSR2:
			this.safetyCall(this.signalUsr2)
		case syscall.SIGHUP:
			// we must ignore SIGHUP because when
			// the session leader terminates we didn't want to exit
		}
	}
}

func (this *Signal) pidfile() string {
	return path.Join(this.execDir, this.execFile+".pid")
}

func (this *Signal) safetyCall(c func()) {
	if c == nil {
		return
	}
	defer func() {
		if err := recover(); err != nil {
			if this.logger == nil {
				return
			}
			this.logger.Error("Err: %s", err)
			for i := 0; i < 10; i++ {
				pc, file, line, ok := runtime.Caller(i)
				if ok {
					p := strings.Index(file, "/src/")
					if p != -1 {
						file = file[p+len("/src/"):]
					}
					this.logger.Error("frame %d:[func:%s,file:%s,line:%d]",
						i, runtime.FuncForPC(pc).Name(), file, line)
				}
			}
		}
	}()
	c()
}

func Kill(s syscall.Signal) error {
	sig := NewSignal()
	pid, err := ioutil.ReadFile(sig.pidfile())
	if err != nil {
		return err
	}
	return exec.Command("kill",
		fmt.Sprintf("-%d", s),
		fmt.Sprintf("%s", string(pid))).Run()
}
