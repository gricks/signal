package main

import (
	"fmt"
	"os"
	"syscall"
	"time"

	"gitee.com/gricks/logrus"
	"gitee.com/gricks/signal"
)

func main() {
	sig := signal.NewSignal(
		signal.WithDup(true),
		signal.WithLogger(logrus.GetEntry()),
		signal.WithSignalTerm(func() {
			fmt.Println("receive SIGTERM")
			fmt.Fprintln(os.Stderr, "stderr output")
			panic("SignalTerm Panic")
		}),
		signal.WithSignalUsr1(func() {
			fmt.Println("receive SIGUSR1")
		}),
		signal.WithSignalUsr2(func() {
			fmt.Println("receive SIGUSR2")
		}),
	)

	go func() {
		time.Sleep(100 * time.Millisecond)
		signal.Kill(syscall.SIGHUP) // ignored

		time.Sleep(100 * time.Millisecond)
		signal.Kill(syscall.SIGUSR1)

		time.Sleep(100 * time.Millisecond)
		signal.Kill(syscall.SIGUSR2)

		//time.Sleep(100 * time.Millisecond)
		//signal.Kill(syscall.SIGTERM)
	}()

	go func() {
		time.Sleep(3 * time.Second)
		//		panic("unexpected panic")
	}()

	sig.Handle()
}
